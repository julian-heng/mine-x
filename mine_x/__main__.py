#!/usr/bin/env python3

from .core.app import App


if __name__ == "__main__":
    app = App.make_instance()
    app.run()
