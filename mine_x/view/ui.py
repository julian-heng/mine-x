#!/usr/bin/env python3

from abc import ABC, abstractmethod


class UserInterface(ABC):

    def __init__(self, app):
        self._app = app

    @abstractmethod
    def loop(self):
        pass

    @abstractmethod
    def left(self):
        pass

    @abstractmethod
    def down(self):
        pass

    @abstractmethod
    def up(self):
        pass

    @abstractmethod
    def right(self):
        pass

    @abstractmethod
    def click(self):
        pass

    @abstractmethod
    def flag(self):
        pass
