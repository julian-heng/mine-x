#!/usr/bin/env python3

import os

from time import sleep

from asciimatics.event import KeyboardEvent
from asciimatics.screen import ManagedScreen

from .ui import UserInterface


class ConsoleInterface(UserInterface):

    def __init__(self, app):
        super(ConsoleInterface, self).__init__(app)

        self._cursor_x = 0
        self._cursor_y = 0

    def loop(self):
        with ManagedScreen() as screen:
            while (
                    not self._app.game_over
                    and not self._app.quit
                    and not self._app.game_win
            ):
                self._render(screen)
                evt = screen.get_event()
                if evt is not None and isinstance(evt, KeyboardEvent):
                    code = evt.key_code
                    if code == ord("h"):
                        self.left()
                    elif code == ord("j"):
                        self.down()
                    elif code == ord("k"):
                        self.up()
                    elif code == ord("l"):
                        self.right()
                    elif code == ord("f"):
                        self.flag()
                    elif code == ord("q"):
                        self._app.quit = True
                    elif code == ord(os.linesep[:1]):
                        self.click()

                sleep(0.05)

        if self._app.game_over:
            print("Game Over!")
        elif self._app.quit:
            print("Quit.")
        else:
            print("You Win!")

    def left(self):
        self._cursor_x = max(0, self._cursor_x - 1)

    def down(self):
        self._cursor_y = min(self._app.board.height - 1, self._cursor_y + 1)

    def up(self):
        self._cursor_y = max(0, self._cursor_y - 1)

    def right(self):
        self._cursor_x = min(self._app.board.width - 1, self._cursor_x + 1)

    def click(self):
        if not self._app.board.click(self._cursor_x, self._cursor_y):
            self._app.game_over = True

    def flag(self):
        self._app.board.flag(self._cursor_x, self._cursor_y)

    def _render(self, screen):
        board = self._app.board

        screen.clear()
        self._render_board(screen, board)
        screen.refresh()
        screen.clear_buffer(screen.COLOUR_WHITE, screen.A_NORMAL,
                            screen.COLOUR_BLACK)

    def _render_board(self, screen, board):
        sep = ("+---" * board.width) + "+"
        screen.print_at(sep, 0, 0)
        for y, r in enumerate(board):
            for x, c in enumerate(r):
                draw_x = x * 4 + 2
                draw_y = y * 2 + 1

                if x == 0:
                    screen.print_at("|", 0, draw_y)

                col = screen.COLOUR_WHITE
                attr = screen.A_NORMAL
                if x == self._cursor_x and y == self._cursor_y:
                    attr = screen.A_REVERSE

                screen.print_at(c.rendered_string, draw_x, draw_y, col, attr)
                screen.print_at("|", draw_x + 2, draw_y)

            screen.print_at(sep, 0, y * 2)
        screen.print_at(sep, 0, board.height * 2)
