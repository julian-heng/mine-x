#!/usr/bin/env python3

import random


class Board:

    def __init__(self, width=10, height=10, num_mines=15):
        self._width = width
        self._height = height
        self._num_mines = num_mines
        self._board = Board.make_board(self._width,
                                       self._height,
                                       self._num_mines)

    def __getitem__(self, key):
        return self._board[key]

    def __iter__(self):
        return iter(self._board)

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def num_mines(self):
        return self._num_mines

    def click(self, x, y):
        tile = self._board[y][x]

        if tile.is_flagged or tile.revealed:
            return True

        if tile.is_mine:
            return False

        tile.revealed = True
        if tile.adjacent == 0:
            Board.expand_at(self._board, x, y, list())

        return True

    def flag(self, x, y):
        tile = self._board[y][x]
        tile.is_flagged = not tile.is_flagged

    def check_win(self):
        for row in self._board:
            for col in row:
                if not col.is_mine and not col.is_flagged and not col.revealed:
                    return False
        return True

    @staticmethod
    def make_board(width, height, num_mines):
        size = width * height

        _board = [Tile() for _ in range(size)]
        for i in range(num_mines):
            _board[i].is_mine = True
        random.shuffle(_board)

        board = list()
        for i in range(0, size, width):
            row = list()
            for t in _board[i:i+width]:
                row.append(t)
            board.append(row)

        for y, row in enumerate(board):
            for x, col in enumerate(row):
                adjacent = Board.get_adjacent(board, x, y)
                col.adjacent = sum(board[y][x].is_mine for x, y in adjacent)
        return board

    @staticmethod
    def get_adjacent(board, _x, _y):
        xmin = _x - 1
        xmax = _x + 1
        ymin = _y - 1
        ymax = _y + 1
        adjacent = list()
        for y in range(ymin, ymax + 1):
            for x in range(xmin, xmax + 1):
                if 0 <= y < len(board) and 0 <= x < len(board[y]):
                    adjacent.append((x, y))

        return adjacent

    @staticmethod
    def expand_at(board, _x, _y, seen):
        adjacent = Board.get_adjacent(board, _x, _y)
        seen.append((_x, _y))
        for x, y in adjacent:
            if (x, y) in seen:
                continue
            tile_adjacent = Board.get_adjacent(board, x, y)
            tile = board[y][x]
            if (
                    (x, y) not in seen
                    and not tile.revealed
                    and not tile.is_mine
                    and not tile.is_flagged
                    and any(
                        board[y2][x2].adjacent == 0 for x2, y2 in tile_adjacent
                    )
            ):
                tile.revealed = True
                if tile.adjacent == 0:
                    Board.expand_at(board, x, y, seen)


class Tile:

    def __init__(self):
        self._revealed = False
        # self._revealed = True
        self._adjacent = 0

        self._is_mine = False
        self._is_flagged = False

    @property
    def revealed(self):
        return self._revealed

    @revealed.setter
    def revealed(self, value):
        self._revealed = value

    @property
    def adjacent(self):
        return self._adjacent

    @adjacent.setter
    def adjacent(self, value):
        self._adjacent = value

    @property
    def is_mine(self):
        return self._is_mine

    @is_mine.setter
    def is_mine(self, value):
        self._is_mine = value

    @property
    def is_flagged(self):
        return self._is_flagged

    @is_flagged.setter
    def is_flagged(self, value):
        self._is_flagged = value

    @property
    def rendered_string(self):
        if self._revealed:
            if self._is_mine:
                return "M"
            elif self._adjacent > 0:
                return str(self._adjacent)
            else:
                return " "
        elif self._is_flagged:
            return "F"
        else:
            return "#"
