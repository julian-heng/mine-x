#!/usr/bin/env python3

from ..view.tui import ConsoleInterface
from .board import Board


class App:

    def __init__(self, ui_class, board):
        self._board = board
        self._game_over = False
        self._quit = False

        self._ui = ui_class(self)

    @property
    def board(self):
        return self._board

    @property
    def game_over(self):
        return self._game_over

    @game_over.setter
    def game_over(self, value):
        self._game_over = value

    @property
    def quit(self):
        return self._quit

    @quit.setter
    def quit(self, value):
        self._quit = value

    @property
    def game_win(self):
        return self._board.check_win()

    def run(self):
        self._ui.loop()

    @staticmethod
    def make_instance():
        ui = ConsoleInterface
        board = Board()
        app = App(ui_class=ui, board=board)
        return app
